$(document).ready(function(){
  "use strict";

  // element to detect scroll direction of
  var el = $(window),

  // initialize last scroll position
  lastY = el.scrollTop();

  el.on('scroll', function() {
    // get current scroll position
    var currY = el.scrollTop(),

    // determine current scroll direction
    y = (currY > lastY) ? 'down' : ((currY === lastY) ? 'none' : 'up');

    if ( $(this).scrollTop() > 90 ) {
      if ( y === 'down' ) {
        $('.header-mobile').addClass('nav-up')
      } else {
        $('.header-mobile').removeClass('nav-up')
      }
    }

    // update last scroll position to current position
    lastY = currY;
  });

  // Show hide menu languages
  $('.m-menu-btn').on('click', function(e){
    e.preventDefault();
    $('#menu-languages').toggleClass('active');
  })
  // Show hide menu search
  $('.m-menu-search').on('click', function(e){
    e.preventDefault();
    $('#menu-search').toggleClass('active');
  })
  $('.menu-search').on('click', function(e){
    e.preventDefault();
    $('#box-menu-search').addClass('active');
  })
  $('.search-close').on('click', function(e){
    e.preventDefault();
    $('#box-menu-search').removeClass('active');
  })

  // Most read
  $('.most-read-carousel').owlCarousel({
    loop:false,
    margin:60,
    nav:false,
    dots: true,
    mouseDrag: false,
    touchDrag: true,
    responsive:{
      0:{
        items:1
      },
      767:{
        items:2
      },
      991:{
        items:4
      }
    }
  })

  // Ads
  $('.ads-carousel').owlCarousel({
    loop:false,
    margin:60,
    nav:false,
    dots: true,
    mouseDrag: false,
    touchDrag: true,
    autoplay: true,
    responsive:{
      0:{
        items:1
      },
      767:{
        items:2,
        margin:30,
      },
      991:{
        items:3
      }
    }
  })

  // Intervention
  $('.intervention-carousel').owlCarousel({
    loop:false,
    autoplay: true,
    margin:20,
    nav:false,
    dots: true,
    mouseDrag: false,
    touchDrag: true,
    items: 1
  })



  // Animate
  function animate(){
    $('.animate').each(function(){

      var posItem;
      if ($(window).width() > 767) {
        posItem = $(this).offset().top + 200;
      } else {
        posItem = $(this).offset().top + 50;
      }

      if(posItem > $(window).scrollTop() && posItem < $(window).scrollTop() + $(window).height()){
        $(this).addClass('active');
        $(this).addClass($(this).data("animate"));
      }
    });
    $('.animate-now').each(function(){
      var posItem = $(this).offset().top;

      if(posItem > $(window).scrollTop() && posItem < $(window).scrollTop() + $(window).height()){
        $(this).addClass('active');
        $(this).addClass($(this).data("animate"));
      }
    });
  }

  // Fire animations
  animate();
  $(window).on('scroll', function(){
    animate();
  });


  $(function(){
    $(window).scroll(function () {
      if ($(this).scrollTop() > 150) {
        $('.header-desk').addClass("header-fixed");
      } else {
        $('.header-desk').removeClass("header-fixed");
      }
    });
  });


  $(".video-play").on("click", function(){
    $(".video-play").addClass("hide");

    var urlVideo = $("#video_play").attr("url"),
    iframeUrl = urlVideo + "?rel=0&autoplay=1&autohide=1";

    $(".embed-responsive-item").attr("src", iframeUrl);

  });

  $('.owl-carousel-interna').owlCarousel({
      loop:true,
      margin:10,
      items: 1,
      mouseDrag: false,
      touchDrag: true,
      responsive:{
        0:{
          nav:false,
          dots: true,
        },
        991:{
          nav:true,
          dots: false,
        }
      }
  })


  // Footer go to top
  $(document).on('click', '.footer-back', function(e){
    e.preventDefault();
    $('html, body').animate({ scrollTop: 0 }, 600);
  })

});
